Contains code for implementations of naive Bayes and logistic regression.  Designed to read in data from .csv training and test data files, as well as several parameters (a beta prior for naive Bayes and a learning rate and regularizer for logistic regression).  Both write their defined outputs to files entered as the last command line argument.  

Language: Java
Last modified: 2/20/2015
