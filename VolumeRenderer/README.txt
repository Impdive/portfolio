This code contains the volume-renderer I wrote using 3D texture for a rectilinear grid-based dataset meant to allow the visualization of a supernova.  Some code was copied or modified from examples used by the professor in his graphics course; these have been marked.  This code contains multiple, commented-out applications of various rotations and color transfer functions, as my the video for my project depicted the camera rotating around the supernova while the colors gradually changed.  Videos are available on Google Drive if desired.

Language: C++
Last modified: 12/9/2014